import { Module } from '@nestjs/common';
import { TaskController } from './task.controller';
import { TaskService } from './task.service';
import { TypegooseModule } from 'nestjs-typegoose';
import { Task } from './task.model';

@Module({
  imports: [
    TypegooseModule.forFeature([Task]),
  ],
  controllers: [TaskController],
  providers: [TaskService]
})
export class TaskModule {}
