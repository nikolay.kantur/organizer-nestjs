import { Controller, Get, Param, Query, Post, Body, Delete, Put, Patch } from '@nestjs/common';
import { TaskService } from './task.service';
import { Task } from './task.model';
import * as moment from 'moment';

@Controller('task')
export class TaskController {
	constructor(private readonly taskService: TaskService) {}

	@Get('month-by-days-has-tasks')
	async monthByDaysHasTasks(@Query('day') date: string) {
		if (!date) return [];
		const parsedDate = moment(date);
		if (!parsedDate.isValid()) return [];
		return await this.taskService.getMonthByDaysHasTasks(parsedDate);
	}

	@Get()
	async all(@Query('date') date: string, @Query('beforeDate') beforeDate: string, @Query('done') done: string) {
		if (date) {
			return await this.taskService.getAllByDate(date);
		}
		if (beforeDate && (done === 'false')) {
			return await this.taskService.getAllExpired(beforeDate);
		}

		return [];// await this.taskService.getAll(); 
	}

	@Get(':id')
	async getById(@Param('id') id: string) {
		console.log('123', id);
		return await this.taskService.getById(id);
	}

	@Post()
	async create(@Body() task: Task) {
		try {
			return await this.taskService.createTask(task)
		} catch(e) {
			return e;
		}
	}

	@Delete(':id')
	async deleteById(@Param('id') id: string) {
		return await this.taskService.deleteById(id);
	}

	@Put(':id')
	async putById(@Param('id') id: string, @Body() task: Task) {
		console.log(task);
		return await this.taskService.putById(id, task);
	}

	@Patch(':id')
	async patchById(@Param('id') id: string, @Body() task: Partial<Task>) {
		console.log(task);
		return await this.taskService.patchById(id, task);
	}
}
