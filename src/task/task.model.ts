import { prop, Typegoose } from '@typegoose/typegoose';

export class Task extends Typegoose {

    @prop({ required: true })
    title?: string;

    @prop({ required: true })
    date?: string;

    @prop()
    done?: boolean;

    @prop()
    order?: number;
}