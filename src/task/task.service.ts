import { Injectable, BadRequestException } from '@nestjs/common';
import { Task } from './task.model';
import { InjectModel } from 'nestjs-typegoose';
import { ReturnModelType } from '@typegoose/typegoose';
import moment = require('moment');

@Injectable()
export class TaskService {
    constructor(
        @InjectModel(Task) private readonly taskModel: ReturnModelType<typeof Task>
    ) {}

    async createTask(task: Task): Promise<Task> {
        const createdTask = new this.taskModel(task);
        return await createdTask.save();
    }

    async getAll(): Promise<Task[] | null> {
        return await this.taskModel.find().exec();
    }

    async getAllExpired(date): Promise<Task[] | null> {
        return await this.taskModel.find({
            done: { $ne: true },
            date: { $lt: date },
        }, null);
    }

    async getAllByDate(date: string): Promise<Task[] | null> {
        return await this.taskModel.find({date: new RegExp(date)}).exec();
    }

    async getById(id: string): Promise<Task | null> {
        this.checkId(id);
        return await this.taskModel.findById(id);
    }

    async deleteById(id:string): Promise<Task | null> {
        this.checkId(id);
        return await this.taskModel.findByIdAndDelete(id);
    }

    async putById(id: string, task: Task): Promise<Task | null> {
        this.checkId(id);
        return await this.taskModel.findByIdAndUpdate(id, task, {new: true});
    }

    async patchById(id: string, task: Partial<Task>): Promise<Task | null> {
        this.checkId(id);
        return await this.taskModel.findByIdAndUpdate(id, task);
    }

	async getMonthByDaysHasTasks(date: moment.Moment) { // TODO add Promise< something >
        const firstDate = date.clone().startOf('month').startOf('isoWeek').format('YYYY-MM-DD');
        const lastDate = date.clone().endOf('month').endOf('isoWeek').format('YYYY-MM-DD');

        console.log({ date: date.format('YYYY-MM-DD'), firstDate, lastDate })

        return await this.taskModel.aggregate([
            // done: { $ne: true },

            {
                $match: {
                    date: {
                        $gte: firstDate,
                        $lte: lastDate,
                    },
                    // done: { $ne: true },
                }
            },
            // {
            //     $project: {
            //         taskDone: { $cond: [ "$done"] }
            //     }
            // },
            {
                $group: {
                    _id: "$date",
                    tasks: { $sum: 1 },
                    tasksUndone: { $sum: { $cond: [
                        { $ne: ['$done', true ] },
                        1,
                        0,
                    ]}},
                    tasksDone: { $sum: { $cond: [
                        { $eq: ['$done', true ] },
                        1,
                        0,
                    ]}},
                    // tasksDone: {
                    //     $sum: { }
                    // }
                    // tasksUndone: false,
                    // tasksDone: false,
                    
                },
            },
            // {
            //     $sort: "$_id"
            // }
        ], console.log);
	}

    isId(id: string): boolean{
        return id.search(/^[0-9a-fA-F]{24}$/) !== -1;
    }

    checkId(id: string): void { // TODO convert to decorator
        if (!this.isId(id)) {
            throw new BadRequestException('Wrong Id')
        };
    }
}
