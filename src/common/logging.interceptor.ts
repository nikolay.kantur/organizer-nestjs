import { Injectable, NestInterceptor, ExecutionContext, CallHandler } from '@nestjs/common';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

@Injectable()
export class LoggingInterceptor implements NestInterceptor {
  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    console.log();
    const ctx = context.switchToHttp();
    const request = ctx.getRequest<Request>();
    console.log('\x1b[32m%s\x1b[0m %s\x1b[36m %s\x1b[0m', context.getClass().name, request.method, request.url);

    const now = Date.now();
    return next
      .handle()
      .pipe(
        tap(() => console.log('\x1b[32m%s\x1b[0m', context.getClass().name, `${Date.now() - now}ms`)),
      );
  }
}